#[derive(Clone, Copy)]
pub enum Color {
    Black,
    White,
    Red,
    Blue,
    Orange,
    Green,
    Yellow,
    Brown,
}

pub fn color_to_array(color: Color) -> [f32; 3] {
    match color {
        Color::Black  => {BLACK}
        Color::White  => {WHITE}
        Color::Red    => {RED}
        Color::Blue   => {BLUE}
        Color::Orange => {ORANGE}
        Color::Green  => {GREEN}
        Color::Yellow => {YELLOW}
        Color::Brown  => {BROWN}
    }
}

pub const BLACK:  [f32; 3] = [0.0, 0.0, 0.0];
pub const WHITE:  [f32; 3] = [1.0, 1.0, 1.0];
pub const RED:    [f32; 3] = [1.0, 0.0, 0.0];
pub const BLUE:   [f32; 3] = [0.0, 0.0, 1.0];
pub const ORANGE: [f32; 3] = [1.0, 0.5, 0.0];
pub const GREEN:  [f32; 3] = [0.0, 1.0, 0.0];
pub const YELLOW: [f32; 3] = [1.0, 1.0, 0.0];
pub const BROWN:  [f32; 3] = [0.2, 0.1, 0.0];
