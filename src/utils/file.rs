/*
use std::fs::File;
use std::io::BufReader;
use obj::ObjData;
use crate::graphics::renderer::Vertex;

/// Returns a vertex buffer that should be rendered as `TrianglesList` from raw input.
pub fn load_obj_raw(mut input: BufReader<File>) -> Vec<Vertex> {
    let data = ObjData::load_buf(&mut input).unwrap();

    let mut vertex_data = Vec::new();

    for object in data.objects.iter() {
        for polygon in object.groups.iter().flat_map(|g| g.polys.iter()) {
            match polygon {
                obj::SimplePolygon(indices) => {
                    for v in indices.iter() {
                        let position = data.position[v.0];
                        let texture = v.1.map(|index| data.texture[index]);
                        let normal = v.2.map(|index| data.normal[index]);

                        let texture = texture.unwrap_or([0.0, 0.0]);
                        let normal = normal.unwrap_or([0.0, 0.0, 0.0]);

                        vertex_data.push(Vertex {
                            position,
                            normal,
                            texture,
                        })
                    }
                },
            }
        }
    }

    vertex_data
}

/// Returns a vertex buffer that should be rendered as `TrianglesList` from a file.
pub fn load_obj_file(file_name: String) -> Vec<Vertex> {
    //TODO: add error message if file is invalid
    let input = BufReader::new(File::open(file_name).unwrap());
    load_obj_raw(input)
}
*/