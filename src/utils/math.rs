use cgmath::num_traits::{PrimInt};

pub(crate) fn degree_to_under_360<T>(rot: T) -> T
    where T: PrimInt
{
    let max_rot = T::from(360).unwrap();
    ((rot % max_rot) + max_rot) % max_rot
}

#[test]
fn test_degree_to_under_360() {
    if (degree_to_under_360(360) != 0) & (degree_to_under_360(360) != 360) {
        panic!("degree_to_under_360 doesn't seem to be working, 360* = {}* according to the function", degree_to_under_360(360))
    }

    if degree_to_under_360(-20) != 340 {
        panic!("degree_to_under_360 doesn't seem to be working, -20* = {}* according to the function", degree_to_under_360(-20))
    }
}