use std::ops::{Rem, Add};

pub(crate) mod system;
pub(crate) mod file;
pub(crate) mod math;
pub mod color;

/// Trait that describes modulo operation
/// # Examples
///
/// ```¨
/// use tulpen::utils::Mod;
///
/// fn main() {///
///     assert_eq!(4, (-2).modulo(6));
///     assert_eq!(2, 9.modulo(7));
/// }
/// ```
pub trait Mod<RHS=Self> {
    type Output;
    fn modulo(self, rhs: RHS) -> Self::Output;
}

impl<A, B, C> Mod<B> for A where A: Rem<B, Output=C>, B: Clone, C: Add<B, Output=C> + Default + PartialOrd {
    type Output = C;
    fn modulo(self, rhs: B) -> Self::Output {
        let result = self % rhs.clone();
        if result < Self::Output::default() {
            result + rhs
        } else {
            result
        }
    }
}

#[cfg(test)]
mod test {
    use crate::utils::Mod;

    #[test]
    fn test_degree_conversion_formula() {
        let n: i32 = (((800.modulo(360))+360) as i32).modulo(360);

        if n != 80 {
            panic!("Modulo doesn't work correctly!")
        }
    }
}

