#![allow(dead_code)]

#[macro_use]
extern crate derive_builder;

#[macro_use]
extern crate log;

#[cfg(feature = "opengl")]
#[macro_use]
extern crate glium;
extern crate core;

use std::fmt;
use std::num::ParseIntError;
use std::str::FromStr;
use crate::graphics::renderer::RenderAPI;

pub mod engine;
pub mod utils;
pub mod graphics;
pub mod scene;

pub(crate) const  ENGINE_NAME: &str = "Tulpen";
pub(crate) const ENGINE_VERGION: Version = Version {
    major: 0,
    minor: 1,
    patch: 0
};

/// Way to save version data for our engine and the app. Represents an API version of Vulkan.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Version {
    /// Major version number.
    pub major: u32,
    /// Minor version number.
    pub minor: u32,
    /// Patch version number.
    pub patch: u32,
}

impl Version {
    pub const ENGINE_VERSION: Version = Version::major_minor(0, 0);

    /// Constructs a `Version` from the given major and minor version numbers.
    #[inline]
    pub const fn major_minor(major: u32, minor: u32) -> Version {
        Version {
            major,
            minor,
            patch: 0,
        }
    }
}

impl Default for Version {
    fn default() -> Self {
        Self {
            major: 1,
            minor: 0,
            patch: 0
        }
    }
}

impl FromStr for Version {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut iter = s.splitn(3, '.');
        let major: u32 = iter.next().unwrap().parse()?;
        let minor: u32 = iter.next().map_or(Ok(0), |n| n.parse())?;
        let patch: u32 = iter.next().map_or(Ok(0), |n| n.parse())?;

        Ok(Version {
            major,
            minor,
            patch,
        })
    }
}

impl fmt::Debug for Version {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{}.{}.{}", self.major, self.minor, self.patch)
    }
}

impl fmt::Display for Version {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, formatter)
    }
}

pub struct AppCreateInfo {
    pub app_name: String,
    pub app_version: Version,
    pub graphics_api: RenderAPI,
    pub log_config: Option<String>,
}

impl AppCreateInfo {

}
