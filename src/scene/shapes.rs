use cgmath::Point3;
use derive_setters::Setters;

use crate::graphics::renderer::Vertex;
use crate::scene::RenderObject;
use crate::utils::{color, Mod};

use super::shader::SolidColorShader;

#[derive(Default, Debug)]
pub enum Shapes2D {
    Triangle,
    #[default]
    Rectangle,
    Circle
}

#[derive(Default, Debug)]
pub enum Shapes {
    TriangleBasedPyramid,
    #[default]
    Cuboid,
    SquareBasedPyramid,
}

/// The builder struct for a 2D shape
#[derive(Setters)]
pub struct ShapeBuilder {
    shape: Shapes,
    position: Point3<f32>,
    width: f32,
    height: f32,
    depth: f32,
    #[setters(rename = "unsave_rotate")]
    rotation: u16,
    color: [f32; 3],
}

impl Default for ShapeBuilder {
    fn default() -> Self {
        Self::new()
    }
}

impl ShapeBuilder {
    /// Inits the builder
    pub fn new() -> Self {
        ShapeBuilder {
            shape: Shapes::default(),
            position: Point3::new(0., 0., 0.),
            width: 1.0,
            height: 1.0,
            depth: 1.0,
            rotation: 0,
            color: color::RED,
        }
    }

    pub fn rotation(mut self, rotation: u16) -> Self {
        self.rotation = (((rotation.modulo(360))+360) as i32).modulo(360) as u16;

        self
    }

    /// Calculates the vertices and indices of the shapes and returns the final shape struct.
    pub fn build(self) -> RenderObject {
        match self.shape {
            Shapes::Cuboid => {
                RenderObject {
                    vertices: vec![
                        Vertex {position: <[f32; 3]>::try_from(self.position).unwrap(),
                                tex_cords: [0., 0.]},
                        Vertex {position: [self.position[0], self.position[1]+self.width, self.position[2]],
                                tex_cords: [0., 0.]},
                        Vertex {position: [self.position[0]+self.height, self.position[1], self.position[2]],
                                tex_cords: [0., 0.]},
                        Vertex {position: [self.position[0]+self.height, self.position[1]+self.width, self.position[2]], 
                                tex_cords: [0., 0.]},
                        Vertex {position: [self.position[0], self.position[1], self.position[2]+self.height], 
                                tex_cords: [0., 0.]},
                        Vertex {position: [self.position[0], self.position[1]+self.width, self.position[2]+self.height], 
                                tex_cords: [0., 0.]},
                        Vertex {position: [self.position[0]+self.height, self.position[1], self.position[2]+self.height], 
                                tex_cords: [0., 0.]},
                        Vertex {position: [self.position[0]+self.height, self.position[1]+self.width, self.position[2]+self.height], 
                                tex_cords: [0., 0.]},
                    ],
                    indices: vec![0,1,2, 1,2,3, 4,5,6, 5,6,7, 0,1,5, 0,5,4, 4,2,0, 4,2,6, 2,3,7, 2,6,7, 1,3,7, 1,5,7],
                    shader_program: Box::new(SolidColorShader::default()),
                }
            }
            _ => {panic!("The shape {:?} is no implemented", self.shape)}
        }
    }
}
