use std::vec;

use crate::graphics::{renderer::Vertex, camera::{Camera, OrthoCamera}};
use self::shader::{UniformArguments, Shader};

pub mod shapes;
pub mod shader;

pub trait Object {
    // Get the vertexes to pass to the render API
    fn get_render_object(&self) -> RenderObject;
}

pub struct ObjectManager {
    pub objects: Vec<RenderObject>,
    pub uniforms: Vec<UniformArguments>,
    pub camera: Box<dyn Camera>,
}

impl Default for ObjectManager {
    fn default() -> Self {
        Self::new()
    }
}

impl ObjectManager {
    pub fn new() -> Self {
        Self {
            objects: vec![],
            uniforms: vec![],
            camera: Box::new(OrthoCamera::new(2., 2.)),
        }
    }

    pub(crate) fn get_view_matrix(&self) -> [[f32; 4]; 4] {
        self.camera.matrix().into()
    }

    pub(crate) fn get_objects(&self) -> &Vec<RenderObject> {
        &self.objects
    }
}

pub struct RenderObject {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,
    pub shader_program: Box<dyn Shader>,
}
