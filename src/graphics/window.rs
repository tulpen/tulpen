#[derive(Copy, Clone)]
pub enum WindowMode {
    Fullscreen,
    BorderlessFullscreen,
    Windowed,
}


impl Default for WindowMode {
    fn default() -> Self { WindowMode::Windowed }
}

/// Used to control the creation of a window
#[derive(Builder, Default, Clone)]
pub struct WindowOptions {
    /// The minimum height in pixels that the screen can become.
    #[builder(default = "None ")]
    pub window_min_height: Option<u32>,
    /// The minimum width in pixels that the screen can become.
    #[builder(default = "None")]
    pub window_min_width: Option<u32>,
    /// The maximum height in pixels that the screen can become.
    #[builder(default = "None")]
    pub window_max_height: Option<u32>,
    /// The maximum width in pixels that the screen can become.
    #[builder(default = "None")]
    pub window_max_width: Option<u32>,
    /// The start height of the window in pixels.
    #[builder(default = "500")]
    pub window_start_height: u32,
    /// The start width of the window in pixels.
    #[builder(default = "1000")]
    pub window_start_width: u32,
    /// The start position of the window in pixels in the x-axis.
    #[builder(default = "500")]
    pub window_start_position_x: i32,
    /// The start position of the window in pixels in the y-axis.
    #[builder(default = "1000")]
    pub window_start_position_y: i32,
    /// Makes the window resizable if true.
    #[builder(default = "true")]
    pub resizable: bool,
    /// Makes the window start maximized if true.
    #[builder(default = "true")]
    pub maximized: bool,
    /// Sets whether the window should have a border, a title bar, etc.
    #[builder(default = "true")]
    pub decorations: bool,
    /// Sets whether or not the window will always be on top of other windows.
    #[builder(default = "true")]
    pub always_on_top: bool,
    /// Makes the window fullscreen, borderless fullscreen or windowed depending on the given enum.
    #[builder(default = "WindowMode::BorderlessFullscreen")]
    pub window_mode: WindowMode,
    /// The title of the window that you can see in the toolbar and if you hover over the window in
    /// taskbar.
    #[builder(default = "\"Game\".to_string()")]
    pub window_title: String,
    /// The target frames per second for your game. It is recomended to set the equal to your
    /// monitors herthz.
    #[builder(default = "60")]
    pub target_fps: u32,
    /// The target updates per second. Every update networking, user input and physics get
    /// calculated. It is recomended to set this higher than the target fps.
    #[builder(default = "100")]
    pub target_ups: u32,
    /// Show to frames per second on the corner of the window. See fps location the select which
    /// corner.
    #[builder(default = "false")]
    pub show_fps: bool,
    /// If true the frames per second will be printed to the console.
    #[builder(default = "false")]
    pub print_fps: bool,
    /// This will show all the triangles from the meshes. For example a plain is made (typicly) out
    /// of 2 triangles.
    #[builder(default = "false")]
    pub show_triangles: bool,
    /// How hard the UI needs to zoom in or out. The default zoom is 5, this is recomended for
    /// people with 1920x1080 resolution.
    #[builder(default = "10")]
    pub zoom_scale: u8,
    /// The quality of the shadow. This goes from 0 to 10 with 10 being the highest quality.
    #[builder(default = "5")]
    pub shadow_quality: u8,
}
